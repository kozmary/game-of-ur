﻿using System.Collections;
using System.Reflection;
using NUnit.Framework;
using src;
using src.UI;
using UnityEditor;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace Tests
{
    public class Tests 
    {
        [UnityTest]
        public IEnumerator TestGamePiece()
        {
            var scoreDisplayPrefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/UI/Score Display.prefab");
            var scoreDisplay = Object.Instantiate(scoreDisplayPrefab);
            scoreDisplay.GetComponent<ScoreDisplay>().PieceToDisplay = Resources.Load<GamePiece>("Pieces/Orange Piece");

            yield return null;

            // This is brittle, but I don't want to public the piece containers just for this test.
            foreach (Transform container in scoreDisplay.transform)
            {
                foreach (Transform sprite in container)
                {
                    var image = sprite.GetComponent<Image>();
                    Debug.Log(image.color);
                    Assert.True(
                        image.color.Equals(scoreDisplay.GetComponent<ScoreDisplay>().PieceToDisplay.spriteTint));
                }
            }
        }
    }
}
