﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace src
{
    public class MenuScroller : MonoBehaviour
    {
        private ScrollRect _scrollRect;
        [SerializeField] private float scrollAnimationDuration = 1.0f;

        private void Awake()
        {
            _scrollRect = GetComponent<ScrollRect>();
        }
    
        public void ScrollToNormalizedPosition(float pos)
        {
            StopAllCoroutines();
            StartCoroutine(ScrollToPos(pos));
        }

        private void OnGUI()
        {
            var text = _scrollRect.normalizedPosition.ToString();
            GUI.Label(new Rect(0,0,100,50), text);
        }

        private IEnumerator ScrollToPos(float pos)
        {
            if (Math.Abs(scrollAnimationDuration) < float.Epsilon)
            {
                _scrollRect.normalizedPosition = new Vector2(0.5f, pos);
                yield break;
            }

            var startTime = Time.time;
            var startPos = _scrollRect.normalizedPosition.y;

            float delta;
            do
            {
                delta = Time.time - startTime;
                // Clamping delta is handled by Mathf.Lerp automatically
                var verticalPos = Mathf.Lerp(startPos, pos, delta / scrollAnimationDuration);
                _scrollRect.normalizedPosition = new Vector2(0.5f, verticalPos);
                yield return null;
            } while (delta < scrollAnimationDuration);
        }
    }
}
