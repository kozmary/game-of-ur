﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class CarouselMenu : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField] private List<string> options;
#pragma warning restore 0649
    private List<ICarouselSelectionResponder> _responders;
    private int _currentSelection;
    // Start is called before the first frame update
    void Start()
    {
        _responders = new List<ICarouselSelectionResponder>(
            GetComponentsInChildren<ICarouselSelectionResponder>());

        foreach (var responder in _responders)
        {
            responder.UpdateSelection(options, 0);
        }
    }

    public void ChangeSelection(int delta)
    {
        _currentSelection += delta;
        while (_currentSelection < 0)
            _currentSelection += options.Count;
        _currentSelection %= options.Count;

        foreach (var responder in _responders)
        {
            responder.UpdateSelection(options, _currentSelection);
        }
    }
}

public interface ICarouselSelectionResponder
{
    void UpdateSelection(List<string> options, int selection);
}
