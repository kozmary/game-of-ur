﻿using System;
using System.Collections;
using UnityEngine;

namespace src
{
    public class CameraViewpointSwitcher : MonoBehaviour
    {
        // This records the state of the transition from white's POV to black's POV
        private float _t;
        #pragma warning disable 0649
        [SerializeField] private AnimationCurve cameraTransitionCurve;
        #pragma warning restore 0649
        private float _animationDuration;

        // Values describe how the camera pivot will be rotated for each player's turn.
        // The starting value is for white's turn, and the inverse rotation will be used for black.
        private Quaternion _whiteRotation;
        private Quaternion _blackRotation;

        public void Awake()
        {
            _animationDuration = cameraTransitionCurve.keys[cameraTransitionCurve.length - 1].time;
            
            _whiteRotation = transform.rotation;
            _blackRotation = Quaternion.Inverse(_whiteRotation);
            
            GameManager.OnTurnBegan += TransitionToPov;
        }

        private void TransitionToPov(bool isWhite)
        {
            if(isWhite)
                TransitionToWhitePov();
            else
                TransitionToBlackPov();
        }

        private void TransitionToWhitePov()
        {
            StopAllCoroutines();
            StartCoroutine(TransitionToTargetValue(0f));
        }

        private void TransitionToBlackPov()
        {
            StopAllCoroutines();
            StartCoroutine(TransitionToTargetValue(_animationDuration));
        }
        private IEnumerator TransitionToTargetValue(float value)
        {
            while (Math.Abs(_t - value) > float.Epsilon)
            {
                var signedDelta = value > _t ? Time.deltaTime : -Time.deltaTime;
                _t = Mathf.Clamp(_t + signedDelta, 0f, _animationDuration);
                var animationCurveValue = cameraTransitionCurve.Evaluate(_t);
                transform.rotation = Quaternion.Slerp(_whiteRotation, _blackRotation, animationCurveValue);
                yield return null;
            }
        }
    }
}
