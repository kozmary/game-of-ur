﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace src.UI
{
    public class MainMenuPieceColorUpdater : MonoBehaviour, IPieceCarouselSelectionResponder
    {
        private PiecePickerCarouselMenu _menu;
    
        public delegate void OnPieceUpdatedDelegate(string piecePath);
        public static event OnPieceUpdatedDelegate OnHomePieceUpdated;
        public static event OnPieceUpdatedDelegate OnAwayPieceUpdated;

        private void Awake()
        {
            _menu = transform.GetComponent<PiecePickerCarouselMenu>();
        }
    
        public void UpdateSelection(IList<GamePiece> pieces, int selection, int opponentSelection)
        {
            if(_menu.isEditingHomePlayer)
                OnHomePieceUpdated?.Invoke(AssetDatabase.GetAssetPath(pieces[selection]));
            else 
                OnAwayPieceUpdated?.Invoke(AssetDatabase.GetAssetPath(pieces[selection]));
        }
    }
}
