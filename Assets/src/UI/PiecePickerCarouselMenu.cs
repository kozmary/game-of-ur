﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;
using UnityEditor;

namespace src.UI
{
    public class PiecePickerCarouselMenu : MonoBehaviour
    {
#pragma warning disable 0649
        [SerializeField] private List<GamePiece> pieces;
        [SerializeField] private TextMeshProUGUI menuText;
#pragma warning restore 0649
        public bool isEditingHomePlayer;
        private IList<IPieceCarouselSelectionResponder> _responders;
        private int _currentSelection;

        // Start is called before the first frame update
        void Start()
        {
            _responders = new List<IPieceCarouselSelectionResponder>(
                GetComponentsInChildren<IPieceCarouselSelectionResponder>());
        }

        private void OnEnable()
        {
            _currentSelection = GetPlayerPieceIndex();
            foreach (var responder in _responders)
                responder.UpdateSelection(pieces, _currentSelection, GetOpponentPieceIndex());
        }

        public void ChangeToPlayer(bool isHomePlayer)
        {
            isEditingHomePlayer = isHomePlayer;
            menuText.text = isEditingHomePlayer ? "Home Player Pieces" : "Away Player Pieces";
        }

        public void ChangeSelection(int delta)
        {
            if (pieces.Count == 0)
            {
                Debug.LogError("Piece Array is empty!");
                return;
            }

            _currentSelection += delta;
            while (_currentSelection < 0)
                _currentSelection += pieces.Count;
            _currentSelection %= pieces.Count;

            foreach (var responder in _responders)
            {
                responder.UpdateSelection(pieces, _currentSelection, GetOpponentPieceIndex());
            }
        }

        public void NextValidPiece()
        {
            var opponentPieceIndex = GetOpponentPieceIndex();
            var offset = (_currentSelection + 1) % pieces.Count == opponentPieceIndex ? 2 : 1;
            ChangeSelection(offset);
        }

        public void PrevValidPiece()
        {
            var opponentPieceIndex = GetOpponentPieceIndex();
            var offset =
                (_currentSelection + pieces.Count - 1) % pieces.Count == opponentPieceIndex ? -2 : -1;
            ChangeSelection(offset);
        }
        
        private int GetPlayerPieceIndex()
        {
            var playerPrefsPath = isEditingHomePlayer ? Constants.HomePiecesPath : Constants.AwayPiecesPath;
            var playerPiecePrefabPath = PlayerPrefs.GetString(playerPrefsPath);
            var piecePrefabPaths = pieces.Select(AssetDatabase.GetAssetPath).ToList();
            return piecePrefabPaths.IndexOf(playerPiecePrefabPath);
        }

        private int GetOpponentPieceIndex()
        {
            var opponentPrefsPath = isEditingHomePlayer ? Constants.AwayPiecesPath : Constants.HomePiecesPath;
            var opponentPiecePrefabPath = PlayerPrefs.GetString(opponentPrefsPath);
            var piecePrefabPaths = pieces.Select(AssetDatabase.GetAssetPath).ToList();
            return piecePrefabPaths.IndexOf(opponentPiecePrefabPath);
        }
        
        public void ConfirmSelection()
        {
            var prefsPath = isEditingHomePlayer ? Constants.HomePiecesPath : Constants.AwayPiecesPath;
            var piecePrefab = pieces[_currentSelection];
            var piecePrefabPath = AssetDatabase.GetAssetPath(piecePrefab);
            PlayerPrefs.SetString(prefsPath, piecePrefabPath);
        }
    }

    public interface IPieceCarouselSelectionResponder
    {
        void UpdateSelection(IList<GamePiece> pieces, int selection, int opponentSelection);
    }
}