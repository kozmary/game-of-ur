﻿using UnityEngine;
using System.Collections.Generic;

namespace src
{
    public class LegalMoveDesignator : MonoBehaviour
    {
        public int spaceNumber;
        [SerializeField] private float lineVerticalOffset = 0.2f;
        private LineRenderer _lineRenderer;
        private ParticleSystem _particleSystem;
        public delegate void SpaceChosenDelegate(int spaceNumber);
        public static event SpaceChosenDelegate OnSpaceChosen;

        private void Awake()
        {
            _lineRenderer = GetComponent<LineRenderer>();
            _particleSystem = GetComponent<ParticleSystem>();
        }

        public void InitForGameStateAndSpaceOnTrack(GameState gameState, int space, List<Transform> track)
        {
            spaceNumber = space;
            if(gameState.Ruleset.IsRosetteSquare(space))
                StartRosetteParticles();
            if(gameState.WouldCapture(space))
                StartCaptureParticles();
            DrawArrowFromOffsetForPlayer(gameState.LastDieResult, track);
        }
        
        public void OnMouseDown()
        {
            OnSpaceChosen?.Invoke(spaceNumber);
        }

        public void OnMouseOver()
        {
            _lineRenderer.enabled = true;
        }

        public void OnMouseExit()
        {
            _lineRenderer.enabled = false;
        }

        private void DrawArrowFromOffsetForPlayer(int offset, List<Transform> activePlayerSpaces)
        {
            var positions = new List<Vector3>();
            _lineRenderer.positionCount = offset + 1;
            for (var i = spaceNumber - offset; i <= spaceNumber; ++i)
            {
                positions.Add(activePlayerSpaces[i].position + lineVerticalOffset * Vector3.up);
            }
            _lineRenderer.SetPositions(positions.ToArray());
            _lineRenderer.enabled = false;
        }

        public void StartRosetteParticles()
        {
            _particleSystem.Play();
        }

        public void StartCaptureParticles()
        {
            var main = _particleSystem.main;
            main.startColor = new Color(1f, 0.4f, 0.15f);
            _particleSystem.Play();
        }
    }
}
