﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuContentSwitcher : MonoBehaviour
{
    [SerializeField] private List<GameObject> _menus;
    private Animator _scrollAnimator;

    private void Start()
    {
        _scrollAnimator = GetComponent<Animator>();
        //RollUpBehavior.OnSwitchMenu += SetActiveMenu;
    }
    
    public void ButtonSwitchTo(int index)
    {
        _scrollAnimator.SetInteger("NextMenu", index);
        _scrollAnimator.Play("RollUpAnimation");
    }

    public void SetActiveMenu()
    {
        var index = _scrollAnimator.GetInteger("NextMenu");
        foreach (var menu in _menus)
            menu.SetActive(false);

        _menus[index].SetActive(true);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
