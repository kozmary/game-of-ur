﻿using System;
using System.Collections;
using src.Players;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace src.UI
{
    public class MainMenu : MonoBehaviour
    {
        public void StartLocalMultiplayer()
        {
            var playerPrefab = Resources.Load<LocalPlayer>("Players/LocalPlayer");
            var homePlayer = Instantiate(playerPrefab);
            homePlayer.isHomePlayer = true;
            DontDestroyOnLoad(homePlayer);

            var awayPlayer = Instantiate(playerPrefab);
            awayPlayer.isHomePlayer = false;
            DontDestroyOnLoad(awayPlayer);

            SceneManager.sceneLoaded += homePlayer.InitializePlayer;
            SceneManager.sceneLoaded += awayPlayer.InitializePlayer;

            StartCoroutine(LoadGameScene(() =>
            {
                SceneManager.sceneLoaded -= homePlayer.InitializePlayer;
                SceneManager.sceneLoaded -= awayPlayer.InitializePlayer;
            }));
        }

        public void StartSingleplayer()
        {
            var localPlayerPrefab = Resources.Load<LocalPlayer>("Players/LocalPlayer");
            var homePlayer = Instantiate(localPlayerPrefab);
            homePlayer.isHomePlayer = true;
            DontDestroyOnLoad(homePlayer);

            var computerPlayerPrefab = Resources.Load<AIPlayer>("Players/AIPlayer");
            var computerPlayer = Instantiate(computerPlayerPrefab);
            computerPlayer.isHomePlayer = false;
            DontDestroyOnLoad(computerPlayer);

            SceneManager.sceneLoaded += homePlayer.InitializePlayer;

            StartCoroutine(LoadGameScene(() =>
            {
                SceneManager.sceneLoaded -= homePlayer.InitializePlayer;
            }));
        }

        private delegate void SceneLoadedCallback();
        private IEnumerator LoadGameScene(SceneLoadedCallback callback)
        {
            var loadOperation = SceneManager.LoadSceneAsync("GameBoard");

            while (!loadOperation.isDone)
                yield return null;

            callback?.Invoke();
        }
    }
}
