﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using src;
using UnityEditor;
using UnityEngine;

public class PiecePickerTable : MonoBehaviour
{
    #pragma warning disable 0649
    [SerializeField] private GameObject tableCellPrefab;
    #pragma warning restore 0649
    private bool _isSettingHomePlayer;
    private List<PiecePickerCell> _cells;
    
    private void Awake()
    {
        _cells = new List<PiecePickerCell>();
        LoadPiecesTable();
    }

    private void LoadPiecesTable()
    {
        var pieces = Resources.LoadAll("Pieces", typeof(GamePiece));
        foreach (var piece in pieces)
        {
            var cell = Instantiate(tableCellPrefab, transform);
            var piecePickerCell = cell.GetComponent<PiecePickerCell>();
            piecePickerCell.InitWithGamePiece((GamePiece)piece, _isSettingHomePlayer);
            _cells.Add(piecePickerCell);
        }
    }
    
    public void SetEditedPlayer(bool isHomePlayer)
    {
        _isSettingHomePlayer = isHomePlayer;
        var playerString = _isSettingHomePlayer ? "home" : "away";
        Debug.LogFormat("Now choosing pieces for {0} player", playerString);
        foreach (var cell in _cells)
            cell.setsHomePlayer = isHomePlayer;
    }
}
