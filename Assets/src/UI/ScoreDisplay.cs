﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace src.UI
{
    public class ScoreDisplay : MonoBehaviour
    {
        public bool IsHomePlayerDisplay => isHomePlayerDisplay;
#pragma warning disable 0649
        [SerializeField] private bool isHomePlayerDisplay;
        [SerializeField] private Transform startingPiecesContainer;
        [SerializeField] private Transform finishedPiecesContainer;
        [SerializeField] private List<Transform> movementDots;
#pragma warning restore 0649

        public GamePiece PieceToDisplay
        {
            get => _pieceToDisplay;
            set
            {
                _pieceToDisplay = value;
                TintSprites();
            }
        }

        private List<GameObject> _startingPieceSprites;
        private List<GameObject> _finishedPieceSprites;
        private GamePiece _pieceToDisplay;

        private void Awake()
        {
            GameManager.OnPiecesMoved += UpdateScoreTrack;
            GameManager.OnDiceRollChanged += UpdateMovementPoints;
            InitPieceLists();
            var preferencesPath = IsHomePlayerDisplay ? Constants.HomePiecesPath : Constants.AwayPiecesPath;
            Debug.LogFormat("Preferences Path: {0}", preferencesPath);
            var prefabPath = PlayerPrefs.GetString(preferencesPath);
            Debug.LogFormat("Loading piece from: {0}", prefabPath);
            PieceToDisplay = AssetDatabase.LoadAssetAtPath<GamePiece>(prefabPath);
        }

        private void InitPieceLists()
        {
            _startingPieceSprites = new List<GameObject>();
            foreach(Transform child in startingPiecesContainer)
                _startingPieceSprites.Add(child.gameObject);
            _finishedPieceSprites = new List<GameObject>();
            foreach(Transform child in finishedPiecesContainer)
                _finishedPieceSprites.Add(child.gameObject);
        }

        private void TintSprites()
        {
            foreach (var sprite in _startingPieceSprites)
            {
                sprite.GetComponent<Image>().color = PieceToDisplay.spriteTint;
            }
            foreach (var sprite in _finishedPieceSprites)
            {
                sprite.GetComponent<Image>().color = PieceToDisplay.spriteTint;
            }
        }

        private void UpdateScoreTrack(GameState gameState)
        {
            var myPieces = IsHomePlayerDisplay ? 
                gameState.HomePieceLocations : 
                gameState.AwayPieceLocations;
            var startingPiecesCount = myPieces.Count(piece => piece == 0);
            var finishedPiecesCount = myPieces.Count(piece => piece == gameState.Ruleset.EndSquare);

            for (var i = 0; i < gameState.Ruleset.NumPieces; ++i)
            {
                _startingPieceSprites[i].SetActive(i < startingPiecesCount);
                _finishedPieceSprites[i].SetActive(i < finishedPiecesCount);
            }
        }

        private void UpdateMovementPoints(GameState gameState)
        {
            for (var i = 0; i < gameState.Ruleset.NumDice; ++i)
            {
                var active = gameState.IsHomeToGo() == isHomePlayerDisplay &&
                                  gameState.LastDieResult > i;
                movementDots[i].gameObject.SetActive(active);
            }
        }
    }
}
