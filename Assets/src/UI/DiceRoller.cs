﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace src
{
    public class DiceRoller : MonoBehaviour
    {
        #pragma warning disable 0649
        [SerializeField] private CanvasGroup popup;
        [SerializeField] private Text resultText;
        [SerializeField] private Text playerTurnIndicatorText;
        #pragma warning restore 0649
        [SerializeField] private float diceRollAnimationDuration = 0.5f;
        private int pendingResult;

        private void Awake()
        {
            GameManager.OnDiceRollInputRequired += PresentDicePopupWithResultForPlayer;
        }

        private void PresentDicePopupWithResultForPlayer(int diceRollResult, bool isWhite)
        {
            StopAllCoroutines();
            pendingResult = diceRollResult;
            popup.alpha = 1f;
            popup.blocksRaycasts = true;
            resultText.text = "";
            playerTurnIndicatorText.text = isWhite ? "White Rolling!" : "Black Rolling!";
        }

        public void RollButtonPressed()
        {
            resultText.text = $"{pendingResult}";
            StartCoroutine(WaitAndHidePopup());
        }

        private IEnumerator WaitAndHidePopup()
        {
            yield return new WaitForSeconds(diceRollAnimationDuration);
            HideDicePopup();
        }

        private void HideDicePopup()
        {
            popup.alpha = 0f;
            popup.blocksRaycasts = false;
        }
    }
}
