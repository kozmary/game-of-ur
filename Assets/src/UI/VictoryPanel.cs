﻿using System.Collections;
using System.Collections.Generic;
using src;
using UnityEngine;
using UnityEngine.UI;

public class VictoryPanel : MonoBehaviour
{
    #pragma warning disable 0649
    [SerializeField] private Text playerNameText;
    #pragma warning restore 0649
    private CanvasGroup _canvasGroup;

    private void Awake()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
        GameManager.OnPlayerVictory += PresentVictoryPanelForPlayer;
        HidePanel();
    }
    
    public void PresentVictoryPanelForPlayer(bool isWhite)
    {
        ShowPanel();
        var player = isWhite ? "White" : "Black";
        playerNameText.text = $"{player} Wins!";
    }

    public void HidePanel()
    {
        _canvasGroup.alpha = 0;
        _canvasGroup.blocksRaycasts = false;
    }

    public void ShowPanel()
    {
        _canvasGroup.alpha = 1;
        _canvasGroup.blocksRaycasts = true;
    }
}
