﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace src.UI
{

    public class PieceDisplayCarousel : MonoBehaviour, IPieceCarouselSelectionResponder
    {
        [SerializeField] private List<Image> pieceImages;

        public void UpdateSelection(IList<GamePiece> options, int selection, int opponentSelection)
        {
            for (var i = -2; i <= 2; i++)
            {
                var idx = selection + i;
                while (idx < 0)
                    idx += options.Count;
                idx %= options.Count;
                var color = options[idx].spriteTint;
                if (idx == opponentSelection)
                    color.a = 0.2f;
                pieceImages[i + pieceImages.Count / 2].color = color;
            }
        }
        
        private static Color ColorFromString(string input)
        {
            var red = input.Substring(0, 2);
            var r = int.Parse(red, System.Globalization.NumberStyles.HexNumber) / 255f;
            var green = input.Substring(2, 2);
            var g = int.Parse(green, System.Globalization.NumberStyles.HexNumber) / 255f;
            var blue = input.Substring(4, 2);
            var b = int.Parse(blue, System.Globalization.NumberStyles.HexNumber) / 255f;

            return new Color(r, g, b, 1f);
        }
    }

}