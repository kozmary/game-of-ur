﻿using src;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class PiecePickerCell : MonoBehaviour
{
    #pragma warning disable 0649
    [SerializeField] private Image piecePreviewImage;
    [SerializeField] private Text pieceDescriptionBlurb;
    [SerializeField] private GamePiece piece;
    #pragma warning restore 0649
    private string _prefabPath;
    public bool setsHomePlayer;
    
    public void InitWithGamePiece(GamePiece gamePiece, bool isHomePlayer)
    {
        piecePreviewImage.color = gamePiece.spriteTint;
        pieceDescriptionBlurb.text = gamePiece.historicalContext;
        _prefabPath = AssetDatabase.GetAssetPath(gamePiece);
        setsHomePlayer = isHomePlayer;
    }

    public void ChoosePiece()
    {
        var preferencesPath = setsHomePlayer ? Constants.HomePiecesPath : Constants.AwayPiecesPath;
        PlayerPrefs.SetString(preferencesPath, _prefabPath);
        Debug.LogFormat("{0} chose pieces: {1}", preferencesPath, _prefabPath);
    }

    public void DisableCell()
    {
        GetComponent<Button>().enabled = false;
        // Should add a gray transparent square on top as well.
    }

    public void EnableCell()
    {
        GetComponent<Button>().enabled = true;
    }
}
