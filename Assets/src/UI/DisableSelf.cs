﻿using System.Collections;
using System.Collections.Generic;
using src;
using UnityEngine;
using UnityEngine.UI;

public class DisableSelf : MonoBehaviour
{
    private Button _button;
    
    private void Awake()
    {
        _button = GetComponent<Button>();
        GameManager.OnDiceRollInputRequired += (int roll, bool isWhite) =>
        {
            _button.enabled = true;
        };
    }

    public void Disable()
    {
        _button.enabled = false;
    }
}
