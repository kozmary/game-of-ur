﻿using System.Collections.Generic;
using src.UI;
using UnityEditor;
using UnityEngine;

namespace src
{
    public class PieceSettingsResponder : MonoBehaviour
    {
        [SerializeField] private bool isHomePlayerPiece;
        private Dictionary<string, Transform> pieces;

        // Start is called before the first frame update
        void Start()
        {
            pieces = new Dictionary<string, Transform>();
            var prefsPath = isHomePlayerPiece ? Constants.HomePiecesPath : Constants.AwayPiecesPath;
            SpawnPiece(PlayerPrefs.GetString(prefsPath));

            if (isHomePlayerPiece)
                MainMenuPieceColorUpdater.OnHomePieceUpdated += SpawnPiece;
            else
                MainMenuPieceColorUpdater.OnAwayPieceUpdated += SpawnPiece;
        }

        private void SpawnPiece(string piecePath)
        {
            if (!pieces.ContainsKey(piecePath))
            {
                var prefab = AssetDatabase.LoadAssetAtPath<Transform>(piecePath);
                pieces[piecePath] = Instantiate(prefab, transform);
            }

            foreach (var kvp in pieces)
                kvp.Value.gameObject.SetActive(false);
            pieces[piecePath].gameObject.SetActive(true);
        }
    }
}