﻿namespace src
{
    public static class Constants
    {
        public const string HomePiecesPath = "HOME_PIECE_PREFAB";
        public const string AwayPiecesPath = "AWAY_PIECE_PREFAB";
        public const string DefaultHomePieces = "Assets/Resources/Pieces/White Piece.prefab";
        public const string DefaultAwayPieces = "Assets/Resources/Pieces/Black Piece.prefab";
    }
}
