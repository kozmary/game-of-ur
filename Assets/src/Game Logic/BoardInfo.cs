﻿namespace src
{
    public abstract class BoardInfo
    {
        public abstract int EndSquare { get; }
        public abstract int NumPieces { get; }
        public abstract int NumDice { get; }
        public abstract int DiceFaces { get; }

        public abstract bool IsRosetteSquare(int square);

        public abstract bool IsInSharedTrack(int square);
    }

    public class FinkelDefault : BoardInfo
    {
        public override int EndSquare => 15;

        public override int NumPieces => 7;
        public override int NumDice => 4;
        public override int DiceFaces => 2;

        public override bool IsRosetteSquare(int square)
        {
            return square == 4 || square == 8 || square == 14;
        }

        public override bool IsInSharedTrack(int square)
        {
            return square > 4 && square < 13;
        }
    }
}