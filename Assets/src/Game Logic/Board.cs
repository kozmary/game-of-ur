﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

// This class is just for handling the positions of game objects corresponding to the game pieces. The actual logic
// for the game (i.e. ensuring moves are legal) is handled by the GameState class.
// This class is responsible for asking the game state object for dice rolls and inputting moves for the right player.
namespace src
{
    public class Board : MonoBehaviour
    {
        #pragma warning disable 0649
        [FormerlySerializedAs("whiteTrackSpaces")]
        [SerializeField] private List<Transform> homeTrackSpaces;
        [FormerlySerializedAs("blackTrackSpaces")]
        [SerializeField] private List<Transform> awayTrackSpaces;
        [SerializeField] private GameObject legalMoveDesignatorPrefab;
        #pragma warning restore 0649
    
        private List<Transform> _homePieces;
        private List<Transform> _awayPieces;
        private List<GameObject> _legalMoveDesignators;

        private void Awake()
        {
            _legalMoveDesignators = new List<GameObject>();
            GameManager.OnPiecesMoved += UpdateAllPieceLocations;
        }

        public void InitializeForRuleset(BoardInfo ruleset)
        {
            var homePiecePrefabPath = PlayerPrefs.GetString(Constants.HomePiecesPath);
            var homePiecePrefab = AssetDatabase.LoadAssetAtPath<Transform>(homePiecePrefabPath);
            var awayPiecePrefabPath = PlayerPrefs.GetString(Constants.AwayPiecesPath);
            var awayPiecePrefab = AssetDatabase.LoadAssetAtPath<Transform>(awayPiecePrefabPath);
            
            _homePieces = new List<Transform>();
            _awayPieces = new List<Transform>();
        
            for (var i = 0; i < ruleset.NumPieces; ++i)
            {
                _homePieces.Add(Instantiate(homePiecePrefab, homeTrackSpaces[0].position, Quaternion.identity));
                _awayPieces.Add(Instantiate(awayPiecePrefab, awayTrackSpaces[0].position, Quaternion.identity));
            }
        }

        public void UpdateAllPieceLocations(GameState gameState)
        {
            CleanupLegalSquareDesignators();
            
            for (var i = 0; i < gameState.Ruleset.NumPieces; ++i)
            {
                var homePiece = _homePieces[i].GetComponent<GamePiece>();
                var awayPiece = _awayPieces[i].GetComponent<GamePiece>();

                var homeDestinationSpaceNumber = gameState.HomePieceLocations[i];
                var awayDestinationSpaceNumber = gameState.AwayPieceLocations[i];

                var homeDestinationPosition = homeTrackSpaces[homeDestinationSpaceNumber].position;
                var awayDestinationPosition = awayTrackSpaces[awayDestinationSpaceNumber].position;
            
                homePiece.MoveTo(homeDestinationPosition);
                awayPiece.MoveTo(awayDestinationPosition);

                var homePieceOnBoard = !(homeDestinationSpaceNumber == 0 ||
                                    homeDestinationSpaceNumber == gameState.Ruleset.EndSquare);
                homePiece.SetVisible(homePieceOnBoard);
                var awayPieceOnBoard = !(awayDestinationSpaceNumber == 0 ||
                                    awayDestinationSpaceNumber == gameState.Ruleset.EndSquare);
                awayPiece.SetVisible(awayPieceOnBoard);
            }
        }

        public void DrawLegalSquares(GameState gameState)
        {
            var activePlayerTrack = gameState.IsHomeToGo() ? homeTrackSpaces : awayTrackSpaces;
            var legalMoves = gameState.LegalMoveDestinations();
            foreach (var move in legalMoves)
            {
                var yOffset = Vector3.up * 0.2f;
                var designator = Instantiate(legalMoveDesignatorPrefab.gameObject,
                    activePlayerTrack[move].position + yOffset,
                    Quaternion.identity);
                
                var legalMoveDesignator = designator.GetComponent<LegalMoveDesignator>();
                legalMoveDesignator.InitForGameStateAndSpaceOnTrack(gameState, move, activePlayerTrack);
                _legalMoveDesignators.Add(designator);
            }
        }

        //@TODO: Make these pooled instead of destroying them
        private void CleanupLegalSquareDesignators()
        {
            foreach(var designator in _legalMoveDesignators)
                Destroy(designator);
        }
    }
}
