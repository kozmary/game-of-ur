﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using Random = System.Random;

namespace src
{
    public class GameState
    {
        public List<int> HomePieceLocations { get; }
        public List<int> AwayPieceLocations { get; }
        private bool _isHomeToGo;
        private readonly Random _rng;
        public int LastDieResult { get; private set; }
        public BoardInfo Ruleset { get; }

        public GameState()
        {
            Ruleset = new FinkelDefault();
            HomePieceLocations = new int[Ruleset.NumPieces].ToList();
            AwayPieceLocations = new int[Ruleset.NumPieces].ToList();
            _isHomeToGo = true;
            _rng = new Random();
            LastDieResult = -1;
        }

        public GameState(GameState stateToCopy)
        {
            HomePieceLocations = new List<int>(stateToCopy.HomePieceLocations);
            AwayPieceLocations = new List<int>(stateToCopy.AwayPieceLocations);
            _isHomeToGo = stateToCopy._isHomeToGo;
            Ruleset = stateToCopy.Ruleset;
        
            // Copying the RNG state is a little more involved. We serialize then deserialize to copy it.
            var binaryFormatter = new BinaryFormatter();
            var stream = new MemoryStream();
            binaryFormatter.Serialize(stream, stateToCopy._rng);
            stream.Position = 0;
            _rng = (Random) binaryFormatter.Deserialize(stream);
        }

        public void Reset()
        {
            for(var i = 0; i < HomePieceLocations.Count; ++i)
                HomePieceLocations[i] = 0;

            for (var i = 0; i < AwayPieceLocations.Count; ++i)
                AwayPieceLocations[i] = 0;

            _isHomeToGo = true;
        }

        // Two game states need not share the same RNG state to be considered equivalent for testing purposes
        public bool Equals(GameState other)
        {
            return HomePieceLocations.SequenceEqual(other.HomePieceLocations) &&
                   AwayPieceLocations.SequenceEqual(other.AwayPieceLocations) &&
                   _isHomeToGo == other._isHomeToGo;
        }

        // Returns a list of legal squares for the active player to move to.
        // Returns null if a dice roll hasn't been generated yet.
        public IEnumerable<int> LegalMoveDestinations()
        {
            if (-1 == LastDieResult)
                return null;

            var legalMoves = new List<int>();
            if (0 == LastDieResult)
                return legalMoves;
    
            var activePieces = _isHomeToGo ? HomePieceLocations : AwayPieceLocations;
            var opponentPieces = _isHomeToGo ? AwayPieceLocations : HomePieceLocations;

            foreach (var piece in activePieces)
            {
                var destination = piece + LastDieResult;
                if (destination > Ruleset.EndSquare)
                    continue;
                if (activePieces.Contains(destination) && destination != Ruleset.EndSquare)
                    continue;
                if (opponentPieces.Contains(destination))
                    if (Ruleset.IsInSharedTrack(destination) && Ruleset.IsRosetteSquare(destination))
                        continue;
                legalMoves.Add(destination);
            }

            return legalMoves;
        }

        public int RollDice()
        {
            if(-1 != LastDieResult)
                Debug.LogErrorFormat("Unconsumed dice roll of : {0}", LastDieResult);
            
            LastDieResult = 0;
            for (var i = 0; i < Ruleset.NumDice; ++i)
            {
                LastDieResult += _rng.Next(0, Ruleset.DiceFaces);
            }
            return LastDieResult;
        }

        public List<int> RollDiceIndividual()
        {
            var result = new List<int>();
            LastDieResult = 0;
            for (var i = 0; i < Ruleset.NumDice; ++i)
            {
                var roll = _rng.Next(0, Ruleset.DiceFaces);
                result.Add(roll);
                LastDieResult +=  roll;
            }

            return result;
        }

        public bool IsHomeToGo()
        {
            return _isHomeToGo;
        }
    
        private void SwitchActivePlayer()
        {
            _isHomeToGo = !_isHomeToGo;
        }

        public bool WouldCapture(int destination)
        {
            var opponentPieces = _isHomeToGo ? AwayPieceLocations : HomePieceLocations;
            return opponentPieces.Contains(destination) && 
                    !Ruleset.IsRosetteSquare(destination) &&
                     Ruleset.IsInSharedTrack(destination);
        }

        // "consumes" the current dice roll to progress the gamestate;
        public bool ProcessMove(int destination)
        {
            if(!ForceMove(LastDieResult, destination))
                return false;
            
            LastDieResult = -1;
            return true;
        }

        public bool PassTurn()
        {
            if (LegalMoveDestinations().Any())
                return false;

            ProcessMove(0);
            return true;
        }

        // Returns true if the move was legal, false otherwise
        public bool ForceMove(int diceResult, int destination)
        {
            var legalMoves = LegalMoveDestinations();

            var legalMovesList = legalMoves.ToList();
            if (!legalMovesList.Any()) {
                SwitchActivePlayer();
                return true;
            }

            if (!legalMovesList.Contains(destination))
                return false;
        
            var activePieces = _isHomeToGo ? HomePieceLocations : AwayPieceLocations;
            var opponentPieces = _isHomeToGo ? AwayPieceLocations : HomePieceLocations;

            var pieceToMove = activePieces.IndexOf(destination - diceResult);
            activePieces[pieceToMove] = destination;
        
            if(opponentPieces.Contains(destination) &&
               Ruleset.IsInSharedTrack(destination) && 
               !Ruleset.IsRosetteSquare(destination))
            {
                var pieceToReturn = opponentPieces.IndexOf(destination);
                opponentPieces[pieceToReturn] = 0;
            }
        
            if (!Ruleset.IsRosetteSquare(destination))
                SwitchActivePlayer();

            return true;
        }

        public bool IsGameOver()
        {
            return HasHomeWon() || HasAwayWon();
        }

        public bool HasHomeWon()
        {
            return HomePieceLocations.All(piece => piece == Ruleset.EndSquare);
        }

        public bool HasAwayWon()
        {
            return AwayPieceLocations.All(piece => piece == Ruleset.EndSquare);
        }

        public bool HasValidDiceRoll()
        {
            return -1 != LastDieResult;
        }
    }
}
