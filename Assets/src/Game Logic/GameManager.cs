﻿using System.Collections;
using System.Linq;
using UnityEngine;

namespace src
{
    public class GameManager : MonoBehaviour
    {
        private GameState _gameState;
        private Board _board;
        public Player homePlayer; // The player for whom the shared track goes right
        public Player awayPlayer; // The player for whom the shared track goes left
        
        // Events
        public delegate void DiceRollInputRequiredDelegate(int diceRollResult, bool isHomePlayer);
        public static event DiceRollInputRequiredDelegate OnDiceRollInputRequired;
        
        public delegate void PlayerVictoryDelegate(bool isHomePlayer);
        public static event PlayerVictoryDelegate OnPlayerVictory;
        
        // Event will be fired any time that any pieces move
        public delegate void PiecesMovedDelegate(GameState gameState);
        public static event PiecesMovedDelegate OnPiecesMoved;
        
        public delegate void TurnBeganDelegate(bool isWhiteTurn);
        public static event TurnBeganDelegate OnTurnBegan;
        
        public delegate void DiceRollUpdatedDelegate(GameState gameState);
        public static event DiceRollUpdatedDelegate OnDiceRollChanged;
    
        // For blocking the game loop until a space has been chosen by a player
        private bool _playerMoveInputReceived; // Used to block the gameloop until a move has been chosen.
        private bool _diceRollInputReceived;
        
        private void Awake()
        {
            _gameState = new GameState();
            _board = FindObjectOfType<Board>();
            _board.InitializeForRuleset(_gameState.Ruleset);
            
            Player.OnPlayerInput += HandlePlayerInputEvent;

            homePlayer = FindObjectsOfType<Player>().First(player => player.isHomePlayer);
            awayPlayer = FindObjectsOfType<Player>().First(player => !player.isHomePlayer);
        }

        private void Start()
        {
            OnPiecesMoved?.Invoke(_gameState);
            StartCoroutine(GameLoop());
        }

        public void ResetGame()
        {
            _gameState.Reset();
            _board.UpdateAllPieceLocations(_gameState);
            StartCoroutine(GameLoop());
        }
        
        private IEnumerator GameLoop()
        {
            while (!_gameState.IsGameOver())
            {
                // Begin Turn Event. Move camera, display popover text.
                OnTurnBegan?.Invoke(_gameState.IsHomeToGo());
            
                // Dice roll
                _gameState.RollDice();
                OnDiceRollInputRequired?.Invoke(_gameState.LastDieResult, _gameState.IsHomeToGo());
                yield return new WaitUntil(() => _diceRollInputReceived);
                OnDiceRollChanged?.Invoke(_gameState); // Even though this is technically generated earlier, wait until player input
                _diceRollInputReceived = false;

                // Check if there are legal moves. If not, end turn.
                var legalMoves = _gameState.LegalMoveDestinations();
                if (!legalMoves.Any())
                {
                    Debug.Log("Active player has no legal moves. Passing Turn.");
                    _gameState.PassTurn();
                    continue;
                }
            
                // Request input from the active player
                var activePlayer = _gameState.IsHomeToGo() ? homePlayer : awayPlayer;
                activePlayer.RequestMoveInput(_gameState);

                // Wait until our OnPlayerInput event handler has run during this iteration of the loop.
                yield return new WaitUntil(() => _playerMoveInputReceived);
                _playerMoveInputReceived = false;
                OnDiceRollChanged?.Invoke(_gameState);

                // Event tells all other components to update (Board, UI, etc.)
                OnPiecesMoved?.Invoke(_gameState);
            }

            var winningPlayerString = _gameState.HasHomeWon() ? "White" : "Black";
            Debug.LogFormat("{0} Wins!", winningPlayerString);
            OnPlayerVictory?.Invoke(_gameState.HasHomeWon());
        }

        public void ReceiveDiceRollInputAfterDelay(float delay)
        {
            StopCoroutine(ReceiveDiceRollCoroutine(delay));
            StartCoroutine(ReceiveDiceRollCoroutine(delay));
        }

        private IEnumerator ReceiveDiceRollCoroutine(float delay)
        {
            yield return new WaitForSeconds(delay);
            _diceRollInputReceived = true;
        }

        private void HandlePlayerInputEvent(object sender, PlayerInputEventArgs args)
        {
            var player = (Player) sender;
            if (player.isHomePlayer != _gameState.IsHomeToGo() ||
                !_gameState.HasValidDiceRoll())
                return; // Ignore input events from inactive player, or if the dice roll has been used
            
            _playerMoveInputReceived = true;
            if(!_gameState.ProcessMove(args.SpaceNumber))
                Debug.LogErrorFormat("Illegal Move Picked");
        }
    }
}
