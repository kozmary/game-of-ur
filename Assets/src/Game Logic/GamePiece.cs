﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

namespace src
{
    public class GamePiece : MonoBehaviour
    {
        public Sprite Uisprite { get; }
        public Color spriteTint;
        [SerializeField] private float animationSpeed = 4f;
        private MeshRenderer _renderer;
        [TextArea(3,10)]
        public string historicalContext;

        private void Awake()
        {
            _renderer = GetComponent<MeshRenderer>();
        }
        
        public void MoveTo(Vector3 worldPosition)
        {
            StopAllCoroutines();
            StartCoroutine(HandleMovement(worldPosition));
        }

        IEnumerator HandleMovement(Vector3 destination)
        {
            while ((transform.position - destination).magnitude > Double.Epsilon)
            {
                transform.position = 
                    Vector3.MoveTowards(transform.position, destination, animationSpeed);
                yield return null;
            }
        }

        public void SetVisible(bool visible)
        {
            _renderer.enabled = visible;
        }
    }
}
