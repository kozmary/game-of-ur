﻿using System;
using src.UI;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace src
{
    public enum PlayerType
    {
        localPlayer,
        networkPlayer,
        AIPlayer
    }
    
    public abstract class Player : MonoBehaviour
    {
        public PlayerType PlayerType { get; }
        public GamePiece playerPieces;
        public bool isHomePlayer;

        public static event EventHandler<PlayerInputEventArgs> OnPlayerInput;

        // Need this to pass events up from derived classes.
        protected virtual void PlayerInputHandler(int move)
        {
            var eventArgs = new PlayerInputEventArgs {SpaceNumber = move};
            OnPlayerInput?.Invoke(this, eventArgs);
        }
        
        public abstract void RequestMoveInput(GameState gameState);

        // arguments are here so that we can be used as a delegate for OnSceneLoad
        public virtual void InitializePlayer(Scene scene, LoadSceneMode mode)
        {
            var piecesPreferencePath = isHomePlayer ? Constants.HomePiecesPath : Constants.AwayPiecesPath;
            var piecesPrefabPath = PlayerPrefs.GetString(piecesPreferencePath);
            playerPieces = AssetDatabase.LoadAssetAtPath<GamePiece>(piecesPrefabPath);
            
            var scoreDisplays = FindObjectsOfType<ScoreDisplay>();
            foreach(var display in scoreDisplays)
                if (display.IsHomePlayerDisplay == isHomePlayer)
                    display.PieceToDisplay = playerPieces;
        }
    }

    public class PlayerInputEventArgs : EventArgs
    {
        public int SpaceNumber { get; set;}
    }
}
