﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace src.Players
{
    public class AIPlayer : Player
    {
        #pragma warning disable 0649
        [SerializeField] private float rosetteCapture;
        [SerializeField] private float captureOpponent;
        [SerializeField] private float captureAvoidance;
        [SerializeField] private float rosetteClearance;
        //[SerializeField] private float 
        #pragma warning restore 0649
        
        public override void RequestMoveInput(GameState gameState)
        {
            PlayerInputHandler(PickBestMove(gameState)); 
        }

        private int PickBestMove(GameState gameState)
        {
            var moveScores = new Dictionary<int,float>();
            foreach (var move in gameState.LegalMoveDestinations())
            {
                var moveScore = 0f;
                if (gameState.Ruleset.IsRosetteSquare(move))
                    moveScore += rosetteCapture;
                if (gameState.WouldCapture(move))
                    moveScore += captureOpponent;
                moveScore -= ProbabilityOpponentReachesSpaceSingleRoll(gameState, move) * captureAvoidance;
                if (gameState.Ruleset.IsRosetteSquare(move - gameState.LastDieResult))
                    moveScore += rosetteClearance;
                
                Debug.LogFormat("Moving to square {0} has score: {1}", move, moveScore);
                moveScores[move] = moveScore;
            }

            if (!moveScores.Any())
                return 0;
            return moveScores.Aggregate((a, b) => a.Value > b.Value ? a : b).Key;
        }

        // Returns the likelihood of an opponent being able to capture your piece if you move it to that space
        // For now we'll ignore multiple moves caused by rosette squares. Need to think a bit more on that algorithm.
        private float ProbabilityOpponentReachesSpaceSingleRoll(GameState gameState, int space)
        {
            if (!gameState.Ruleset.IsInSharedTrack(space))
                return 0f;
            if (!gameState.Ruleset.IsRosetteSquare(space))
                return 0f;

            var opponentPieceLocations = isHomePlayer ?
                gameState.AwayPieceLocations :
                gameState.HomePieceLocations;

            var totalProbabilityOfCapture = 0f;
            var diceRollProbabilities = DiceRollProbabilities(gameState.Ruleset);
            for (var i = 0; i < diceRollProbabilities.Count; ++i)
            {
                var threateningSpace = space - i;
                if (opponentPieceLocations.Contains(threateningSpace))
                    totalProbabilityOfCapture += diceRollProbabilities[i];
            }

            return totalProbabilityOfCapture;
        }

        // Assumes that dice have faces 0 and 1
        // Returns a list where the index is the roll result and the value is the total probability of getting that roll
        private List<float> DiceRollProbabilities(BoardInfo ruleset)
        {
            var probabilities = new List<float>();
            var numDice = ruleset.NumDice;
            var denominator = Mathf.Pow(2, ruleset.NumDice);
            for (var totalResult = 0; totalResult <= numDice; ++totalResult)
                probabilities.Add(BinomialCoefficient(numDice,totalResult)/denominator);
            return probabilities;
        }
        
        // This is wrong for negative numbers, so don't do it!
        private static int PositiveFactorial(int number)
        {
            if (number < 0)
                throw new Exception("Negative number used in PositiveFactorial function.");
            if (number < 2) 
                return 1;
            return number * PositiveFactorial(number - 1);
        }

        private static int BinomialCoefficient(int n, int k)
        {
            if (k > n) throw new Exception("Invalid binomial coefficient requested");
            if (n < 0) throw new Exception("Binomial coefficient n cannot be negative!");
            if (k < 0) throw new Exception("Binomial coefficient k cannot be negative!");
            return PositiveFactorial(n) / (PositiveFactorial(k) * PositiveFactorial(n - k));
        }
    }
}
