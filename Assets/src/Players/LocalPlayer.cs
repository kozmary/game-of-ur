﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace src
{
    public class LocalPlayer : Player
    {
        private Board _board;
        
        private void Awake()
        {
            LegalMoveDesignator.OnSpaceChosen += base.PlayerInputHandler;
        }

        public override void RequestMoveInput(GameState gameState)
        {
            if(_board == null)
                Debug.LogError("Requested moved from uninitialized Player!");
            
            _board.DrawLegalSquares(gameState);
        }

        public override void InitializePlayer(Scene scene, LoadSceneMode mode)
        {
            base.InitializePlayer(scene, mode);
            _board = FindObjectOfType<Board>();
        }
    }
}
