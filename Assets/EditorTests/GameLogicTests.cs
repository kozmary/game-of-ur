﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using src;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class GameLogicTests
    {
        [Test]
        public void TestUnsharedTrack()
        {
            var testState = new GameState();

            Assert.False(testState.Ruleset.IsInSharedTrack(2));

            Assert.True(testState.ForceMove(2, 2)); // White moves a piece to square 2
            Assert.True(testState.ForceMove(2, 2)); // Black moves a piece to square 2

            Assert.Contains(2, testState.HomePieceLocations);
            Assert.Contains(2, testState.AwayPieceLocations);
        }

        [Test]
        public void TestIllegalToCaptureOwnPiece()
        {
            var testState = new GameState();

            Assert.True(testState.Ruleset.IsInSharedTrack(5));
            Assert.True(testState.ForceMove(5, 5));
            Assert.True(testState.ForceMove(0, 0));

            // Save a copy of the game state, and test copy constructor/Equals method
            var stateCopy = new GameState(testState);
            Assert.True(testState.Equals(stateCopy));

            // Attempt an illegal move
            Assert.False(testState.ForceMove(5, 5));

            // Assert that game stat hasn't changed as a result
            Assert.True(testState.Equals(stateCopy));
        }

        [Test]
        public void TestPieceCapture()
        {
            var testState = new GameState();

            Assert.True(testState.ForceMove(5, 5)); // White moves a piece to square 5
            Assert.True(testState.ForceMove(5, 5)); // Black captures the white piece on square 5

            Assert.Contains(5, testState.AwayPieceLocations);
            Assert.True(testState.HomePieceLocations.All(piece => piece == 0));
        }

        [Test]
        public void TestExtraTurn()
        {
            var testState = new GameState();

            Assert.True(testState.Ruleset.IsRosetteSquare(4));
            Assert.True(testState.ForceMove(4, 4));

            Assert.True(testState.IsHomeToGo());
        }

        [Test]
        public void TestTurnOrder()
        {
            var testState = new GameState();

            Assert.True(testState.IsHomeToGo());
            Assert.False(testState.Ruleset.IsRosetteSquare(2));
            Assert.True(testState.ForceMove(2, 2));

            Assert.False(testState.IsHomeToGo());
        }

        [Test]
        public void TestWhiteVictory()
        {
            var testState = new GameState();
            var rs = testState.Ruleset;

            for (var i = 0; i < testState.Ruleset.NumPieces; ++i)
            {
                Assert.True(testState.ForceMove(rs.EndSquare, rs.EndSquare));
                Assert.True(testState.ForceMove(0, 0));
            }

            Assert.True(testState.HomePieceLocations.All(piece => piece == rs.EndSquare));
            Assert.True(testState.AwayPieceLocations.All(piece => piece == 0));
            Assert.True(testState.IsGameOver());
            Assert.True(testState.HasHomeWon());
        }

        [Test]
        public void TestBlackVictory()
        {
            var testState = new GameState();
            var rs = testState.Ruleset;

            for (var i = 0; i < rs.NumPieces; ++i)
            {
                Assert.True(testState.ForceMove(0, 0));
                Assert.True(testState.ForceMove(rs.EndSquare, rs.EndSquare));
            }

            Assert.True(testState.AwayPieceLocations.All(piece => piece == rs.EndSquare));
            Assert.True(testState.HomePieceLocations.All(piece => piece == 0));
            Assert.True(testState.IsGameOver());
            Assert.True(testState.HasAwayWon());
        }

        [Test]
        public void TestWouldCapture()
        {
            var testState = new GameState();

            Assert.False(testState.Ruleset.IsInSharedTrack(2));
            Assert.True(testState.ForceMove(2, 2));
            Assert.False(testState.WouldCapture(2));
            Assert.True(testState.ForceMove(2, 2));
            Assert.True(testState.HomePieceLocations.Contains(2));
            Assert.True(testState.AwayPieceLocations.Contains(2));

            Assert.False(testState.Ruleset.IsRosetteSquare(5));
            Assert.True(testState.ForceMove(3, 5));
            Assert.True(testState.WouldCapture(5));
            Assert.True(testState.ForceMove(3, 5));
            Assert.True(testState.HomePieceLocations.All(piece => piece == 0));
            Assert.True(testState.AwayPieceLocations.Contains(5));
        }

        [Test]
        public void TestReset()
        {
            var testState = new GameState();
            
            Assert.True(testState.HomePieceLocations.All(piece => piece == 0));
            Assert.True(testState.AwayPieceLocations.All(piece => piece == 0));
            
            Assert.False(testState.Ruleset.IsInSharedTrack(2));
            Assert.True(testState.ForceMove(2,2));
            Assert.True(testState.ForceMove(2,2));
            Assert.True(testState.HomePieceLocations.Contains(2));
            Assert.True(testState.AwayPieceLocations.Contains(2));
            
            testState.Reset();
            Assert.True(testState.HomePieceLocations.All(piece => piece == 0));
            Assert.True(testState.AwayPieceLocations.All(piece => piece == 0));
        }
    }
}
